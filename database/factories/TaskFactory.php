<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph(3),
        'status_id' => rand(1, 4),
        'user_id' => 1,
    ];
});
