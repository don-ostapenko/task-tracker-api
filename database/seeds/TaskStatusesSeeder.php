<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'New',
                'code' => 'new',
            ],
            [
                'name' => 'View',
                'code' => 'view',
            ],
            [
                'name' => 'In Progress',
                'code' => 'in_progress',
            ],
            [
                'name' => 'Done',
                'code' => 'done',
            ],
        ];

        DB::table('task_statuses')->insert($data);
    }
}
