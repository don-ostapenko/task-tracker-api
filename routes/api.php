<?php

use App\Http\Controllers\Api\TaskController;
use App\Http\Controllers\Api\TaskStatusController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// task-statuses
Route::post('/task-statuses', [TaskStatusController::class, 'store'])->name('task-statuses.store');

// tasks
Route::patch('/tasks/{task}/patch', [TaskController::class, 'patch'])->name('tasks.patch');
Route::apiResource('/tasks', TaskController::class);

//users
Route::apiResource('/users', UserController::class);
