## About App

This is a "Task tracker API". API tests are implemented. Their statistic is available by the "tests/Coverage" way in HTML format.

Steps of deployment

- Clone the project.
- Run `docker-compose up -d` in root project.
- Run `docker-compose exec php bash`.
- Run `composer install` in a container.
- Run `php artisan migrate:fresh --seed` in a container.

#### REST API
Use Postman for REST API testing.
- x-api-key: `4d0ad2be40aa4b7b8732e7c0b60e7fec`


# USER API
## (GET) Get a list of users
Resource URL
```
http://task-tracker.localhost/api/1.0/users
```
Example Request
```
$ curl --request GET 
    --url 'http://task-tracker.localhost/api/1.0/users' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```
Example Response
```
{
    "message": "OK",
    "data": [
        {
            "id": 1,
            "first_name": "Lane",
            "last_name": "Beatty",
            "email": "kenny02@example.com",
            "created_at": "2020-03-29T11:25:53.000000Z",
            "updated_at": "2020-03-29T11:25:53.000000Z"
        },
        {
            "id": 2,
            "first_name": "Serenity",
            "last_name": "Hamill",
            "email": "christa00@example.org",
            "created_at": "2020-03-29T11:25:53.000000Z",
            "updated_at": "2020-03-29T11:25:53.000000Z"
        },
        ...
    ]
}
```


## (GET) Get specified user
> Return an information about the user specified by the required `id`

Resource URL
```
http://task-tracker.localhost/api/1.0/users/{user}
```

Example Request
```
$ curl --request GET 
    --url 'http://task-tracker.localhost/api/1.0/users/1'
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```

Example Response
```
{
    "message": "OK",
    "data": {
        "id": 1,
        "first_name": "Lane",
        "last_name": "Beatty",
        "email": "kenny02@example.com",
        "created_at": "2020-03-29T11:25:53.000000Z",
        "updated_at": "2020-03-29T11:25:53.000000Z"
    }
}
```


## (POST) Store a newly created user in storage

Name            | Required / Type
----------------|---------------
first_name      | yes / string
last_name       | yes / string
email           | yes / email

Resource URL
```
http://task-tracker.localhost/api/1.0/users
```

Example Request
```
$ curl --request POST 
    --url 'http://task-tracker.localhost/api/1.0/users?first_name=Foo&last_name=Bar&email=foo@bar.com' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


## (PUT/PATCH) Update a user in storage
> Update an information about the user specified by the required `id`

Name            | Required / Type
----------------|---------------
first_name      | - / string
last_name       | - / string
email           | - / email

Resource URL
```
http://task-tracker.localhost/api/1.0/users/{user}
```

Example Request
```
$ curl --request PUT/PATCH 
    --url 'http://task-tracker.localhost/api/1.0/users/1?first_name=Bar' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


## (DELETE) Delete a user from storage
> Delete a user specified by the required `user_id`

Resource URL
```
http://task-tracker.localhost/api/1.0/users/{user}
```

Example Request
```
$ curl --request DELETE 
    --url 'http://task-tracker.localhost/api/1.0/users/1' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


_________________________________________________________________________________________________


# TASK API
## (GET) Get a list of tasks
> Return an information about the tasks specified by the optional `status` and `sort_by`. 

Name            | Value
----------------|---------------
status          | new / view / in_progress / done
sort_by         | id:desc / title:desc / description:desc / ...:asc

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks
```
Example Request
```
$ curl --request GET 
    --url 'http://task-tracker.localhost/api/1.0/tasks?status=done&sort_by=id:desc'
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```
Example Response
```
{
    "message": "OK",
    "data": {
        "19": {
            "id": 20,
            "title": "Itaque hic dolorum in et unde consequatur ea.",
            "description": "Sit quis harum quo doloribus dignissimos ut. Iste non assumenda rem omnis. Quasi dignissimos non sit et illo porro ut illo.",
            "created_at": "2020-03-29T11:25:53.000000Z",
            "updated_at": "2020-03-29T11:25:53.000000Z",
            "status": {
                "id": 4,
                "name": "Done",
                "code": "done"
            },
            "user": {
                "id": 1,
                "first_name": "Lane",
                "last_name": "Beatty",
                "email": "kenny02@example.com",
                "created_at": "2020-03-29T11:25:53.000000Z",
                "updated_at": "2020-03-29T11:25:53.000000Z"
            }
        },
        "18": {
            "id": 19,
            "title": "Labore debitis fugit provident non aut quia qui.",
            "description": "Aut dicta facere non illum. Odit maiores hic ad incidunt voluptatem. Aut blanditiis optio accusamus incidunt velit.",
            "created_at": "2020-03-29T11:25:53.000000Z",
            "updated_at": "2020-03-29T11:25:53.000000Z",
            "status": {
                "id": 4,
                "name": "Done",
                "code": "done"
            },
            "user": {
                "id": 1,
                "first_name": "Lane",
                "last_name": "Beatty",
                "email": "kenny02@example.com",
                "created_at": "2020-03-29T11:25:53.000000Z",
                "updated_at": "2020-03-29T11:25:53.000000Z"
            }
        },
        ...
    ]
}
```


## (GET) Get specified task
> Return an information about the task specified by the required `id`

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks/{task}
```

Example Request
```
$ curl --request GET 
    --url 'http://task-tracker.localhost/api/1.0/tasks/1'
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```

Example Response
```
{
    "message": "OK",
    "data": {
        "id": 1,
        "title": "Consectetur non reiciendis reiciendis commodi et est possimus.",
        "description": "Est totam itaque nobis qui magnam. Iste inventore beatae eos quisquam ut et. Qui dolorem eos soluta laudantium.",
        "created_at": "2020-03-29T11:25:53.000000Z",
        "updated_at": "2020-03-29T11:25:53.000000Z",
        "status": {
            "id": 3,
            "name": "In Progress",
            "code": "in_progress"
        },
        "user": {
            "id": 1,
            "first_name": "Lane",
            "last_name": "Beatty",
            "email": "kenny02@example.com",
            "created_at": "2020-03-29T11:25:53.000000Z",
            "updated_at": "2020-03-29T11:25:53.000000Z"
        }
    }
}
```

## (POST) Store a newly created task in storage

Name            | Required / Type
----------------|---------------
title           | yes / string
description     | yes / string
status_id       | no / int
user_id         | no / int

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks
```

Example Request
```
$ curl --request POST 
    --url 'http://task-tracker.localhost/api/1.0/tasks?title=Foo&description=Bar&status_id=1&user_id=1'
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


## (PUT) Update a task in storage
> Update an information about the task specified by the required `id`

Name            | Required / Type
----------------|---------------
title           | no / string
description     | no / string
status_id       | no / int
user_id         | no / int

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks/{task}
```

Example Request
```
$ curl --request PUT
    --url 'http://task-tracker.localhost/api/1.0/tasks/1?title=Bar' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


## (PATCH) Patch a task's field (-s) in a storage
> Update an information about the "status_id" and "user_id" of task specified by the required `id`

Name            | Required / Type
----------------|---------------
status_id       | no / int
user_id         | no / int

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks/{task}
```

Example Request
```
$ curl --request PATCH
    --url 'http://task-tracker.localhost/api/1.0/tasks/1?status_id=1&user_id=1'
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```


## (DELETE) Delete a task from storage
> Delete a task specified by the required `id`

Resource URL
```
http://task-tracker.localhost/api/1.0/tasks/{task}
```

Example Request
```
$ curl --request DELETE 
    --url 'http://task-tracker.localhost/api/1.0/tasks/1' 
    --header 'x-api-key: <token>'
    --header 'Accept: application/json'
```
