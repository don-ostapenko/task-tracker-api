<?php

namespace App\Providers;

use App\Services\TaskApiService;
use App\Services\UserApiService;
use App\Services\TaskApiServiceInterface;
use App\Services\UserApiServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TaskApiServiceInterface::class, TaskApiService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
