<?php

namespace App\Services;

interface TaskApiServiceInterface
{

    /**
     * Info method.
     *
     * @return string
     */
    public function info(): string;

}
