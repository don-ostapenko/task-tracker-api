<?php

namespace App\Services;

use App\Models\Task;
use Illuminate\Support\Collection;

class TaskApiService implements TaskApiServiceInterface
{

    /**
     * Var that store value for massage.
     *
     * @var Task|Collection $data
     */
    private $data;

    /**
     * Massage of values before a response.
     *
     * @param  array  $parameters
     * @param  object  $data
     *
     * @return mixed
     */
    public function massageRequestValues(array $parameters, object $data): object
    {
        $this->data = $data;

        foreach ($parameters as $parameter => $value) {
            $this->processingParameters($parameter, $value);
        }

        return $this->data;
    }

    /**
     * Processing each parameter received from request.
     *
     * @param  string|null  $parameter
     * @param  string|null  $value
     */
    private function processingParameters($parameter, $value): void
    {
        if ($value) {
            switch ($parameter) {
                case 'status':
                    $this->filterByStatus($value);
                    break;
                case 'sort_by':
                    [$field, $type] = explode(':', $value);
                    $this->sortBy($type, $field);
                    break;
            }
        }
    }

    /**
     * Filter data by status.
     *
     * @param  string  $codeStatus
     */
    private function filterByStatus(string $codeStatus): void
    {
        $this->data = $this->data->filter(function ($entity) use ($codeStatus) {
            return $entity->status->code === $codeStatus;
        });
    }

    /**
     * Sort data by id.
     *
     * @param  string  $type
     * @param  string  $field
     */
    private function sortBy(string $type, string $field): void
    {
        if ($type === 'desc') {
            $this->data = $this->data->sortByDesc($field);
        } elseif ($type === 'asc') {
            $this->data = $this->data->sortBy($field);
        }
    }

    /**
     * Parse response status of data
     * form "status_id" => 1
     * to {
     *      "status" => {"id" => 1, "name" => "View", "code" => "view"},
     *      "user" => {"id" => 1 ...}
     * }.
     *
     * @param $data
     *
     * @return object
     */
    public function parseRepresentationStatusOfResponse($data): object
    {
        // If a received collection of entities, an instance of Collection
        if ($data instanceof Collection) {
            foreach ($data as $entity) {
                $entity->status = $entity->status->get();
                $entity->user = $entity->user ? $entity->user->get() : null;
                unset($entity->status_id);
                unset($entity->user_id);
            }
            return $data;
        }

        // If a received entity, an instance of Entity
        $data->status = $data->status->get();
        $data->user = $data->user ? $data->user->get() : null;
        unset($data->status_id);
        unset($data->user_id);

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function info(): string
    {
        return 'Service for interaction with task controller';
    }

}
