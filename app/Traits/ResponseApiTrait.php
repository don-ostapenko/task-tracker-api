<?php

namespace App\Traits;

trait ResponseApiTrait
{

    /**
     * @param  string  $message
     * @param  int  $status
     * @param  mixed $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse(string $message, int $status = 200, $data = null)
    {
        return response()->json(self::parseResponse($data, $message), $status);
    }

    /**
     * @param $message
     * @param  int  $status
     * @param  array  $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError(string $message, int $status = 400, array $data = [])
    {
        return response()->json(self::parseError($message, $data), $status);
    }

    /**
     * @param  mixed  $data
     * @param  string  $message
     *
     * @return array
     */
    public static function parseResponse($data, string $message): array
    {
        return [
            'message' => $message,
            'data' => $data,
        ];
    }

    /**
     * @param  string  $message
     * @param  array  $data
     *
     * @return array
     */
    public static function parseError(string $message, array $data = []): array
    {
        $result = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($data)) {
            $result['data'] = $data;
        }

        return $result;
    }

}
