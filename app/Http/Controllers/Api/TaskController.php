<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatchTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Services\TaskApiServiceInterface;
use Illuminate\Http\Request;

class TaskController extends Controller implements ApiControllerInterface
{

    /**
     * @var \App\Services\TaskApiServiceInterface
     */
    protected $taskApiService;

    /**
     * @var \App\Models\Task
     */
    protected $model;

    /**
     * TaskController constructor.
     *
     * @param  \App\Services\TaskApiServiceInterface  $taskApiService
     * @param  \App\Models\Task  $model
     */
    public function __construct(TaskApiServiceInterface $taskApiService, Task $model)
    {
        $this->taskApiService = $taskApiService;
        $this->model = $model;
    }

    /**
     * Display tasks of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $entities = $this->model->all();

        if (!$entities->isEmpty()) {
            $entities = $this->taskApiService->massageRequestValues($request->all(), $entities);
            $entities = $this->taskApiService->parseRepresentationStatusOfResponse($entities);
        }

        return $this->sendResponse('OK', 200, $entities);
    }

    /**
     * Store a newly created task in storage.
     *
     * @param  \App\Http\Requests\StoreTaskRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTaskRequest $request)
    {
        $this->model->create($request->validated());
        return $this->sendResponse('Created',201);
    }

    /**
     * Display the specified task.
     *
     * @param  int  $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $entityId)
    {
        $response = [];

        if ($this->model->hasTask('id', $entityId)) {
            $entity = $this->model->find($entityId);
            $response = $this->taskApiService->parseRepresentationStatusOfResponse($entity);
        }

        return $this->sendResponse('OK', 200, $response);
    }

    /**
     * Update the specified task in storage.
     *
     * @param  \App\Http\Requests\UpdateTaskRequest  $request
     * @param  int  $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTaskRequest $request, int $entityId)
    {
        if ($this->model->hasTask('id', $entityId)) {
            $this->model->where('id', $entityId)->update($request->validated());
            return $this->sendResponse('Updated', 204);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

    /**
     * Patch the specified field of task in storage.
     *
     * @param  \App\Http\Requests\PatchTaskRequest  $request
     * @param  int  $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patch(PatchTaskRequest $request, int $entityId)
    {
        if ($this->model->hasTask('id', $entityId)) {
            $this->model->where('id', $entityId)->update($request->validated());
            return $this->sendResponse('Updated', 204);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

    /**
     * Remove the specified task from storage.
     *
     * @param  int  $entityId
     *
     * @return  \Illuminate\Http\JsonResponse
     */
    public function destroy(int $entityId)
    {
        if ($this->model->hasTask('id', $entityId)) {
            $this->model->where('id', $entityId)->delete();
            return $this->sendResponse('Deleted', 204);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

}
