<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

interface ApiControllerInterface
{

    /**
     * Display source of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return  \Illuminate\Http\JsonResponse
     */
    public function index(Request $request);

    /**
     * Display the specified resource.
     *
     * @param  int  $entityId
     *
     * @return  \Illuminate\Http\JsonResponse
     */
    public function show(int $entityId);

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $entityId
     *
     * @return  \Illuminate\Http\JsonResponse
     */
    public function destroy(int $entityId);

}
