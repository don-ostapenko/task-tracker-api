<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStatusTaskRequest;
use App\Models\TaskStatus;

class TaskStatusController extends Controller
{

    /**
     * @var \App\Models\TaskStatus
     */
    protected $model;

    /**
     * TaskStatusController constructor.
     *
     * @param  \App\Models\TaskStatus  $model
     */
    public function __construct(TaskStatus $model)
    {
        $this->model = $model;
    }

    /**
     * Store a newly created status in storage.
     *
     * @param  \App\Http\Requests\StoreStatusTaskRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreStatusTaskRequest $request)
    {
        $this->model->create($request->validated());
        return $this->sendResponse('Created', 201);
    }

}
