<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller implements ApiControllerInterface
{

    /**
     * @var \App\Models\User
     */
    protected $model;

    /**
     * UserController constructor.
     *
     * @param  \App\Models\User  $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Display users of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $entities = $this->model->all();
        return $this->sendResponse('OK', 200, $entities);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreUserRequest $request)
    {
        if (!$this->model->hasUser('email', $request->email)) {
            $this->model->create($request->validated());
            return $this->sendResponse('Created', 201);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

    /**
     * Display the specified user.
     *
     * @param  int  $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $entityId)
    {
        $response = [];

        if ($this->model->hasUser('id', $entityId)) {
            $response = $this->model->find($entityId);
        }

        return $this->sendResponse('OK', 200, $response);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  int  $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, int $entityId)
    {
        if ($this->model->hasUser('id', $entityId)) {
            $this->model->where('id', $entityId)->update($request->validated());
            return $this->sendResponse('Updated', 204);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $entityId
     *
     * @return  \Illuminate\Http\JsonResponse
     */
    public function destroy(int $entityId)
    {
        if ($this->model->hasUser('id', $entityId)) {
            $this->model->where('id', $entityId)->delete();
            return $this->sendResponse('Deleted', 204);
        }

        return $this->sendResponse('Unprocessable entity', 422);
    }

}
