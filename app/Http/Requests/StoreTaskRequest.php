<?php

namespace App\Http\Requests;


class StoreTaskRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'status_id' => 'integer',
            'user_id' => 'integer',
        ];
    }

}
