<?php

namespace App\Http\Requests;

class UpdateTaskRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'description' => 'string',
            'status_id' => 'integer',
            'user_id' => 'integer'
        ];
    }

}
