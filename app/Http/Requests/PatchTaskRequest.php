<?php

namespace App\Http\Requests;

class PatchTaskRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => 'integer',
            'user_id' => 'integer'
        ];
    }
}
