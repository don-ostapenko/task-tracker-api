<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthentication
{

    const API_KEY = 'x-api-key';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header(self::API_KEY);
        $error = [
            'success' => false,
            'message' => 'Unauthorized'
        ];

        if ($token === null) {
            return response()->json($error, 401);
        } elseif ($token !== config('services.api.token')) {
            return response()->json($error, 401);
        }

        return $next($request);
    }

}
