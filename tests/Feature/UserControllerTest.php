<?php

namespace Tests\Feature;

use App\Http\Middleware\ApiAuthentication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserControllerTest extends TestCase
{

    use RefreshDatabase;
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->createUsers();
    }

    /**
     * @test
     */
    public function authenticationFailed()
    {
        $response = $this->get('/api/1.0/users');
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function usersFounded()
    {
        $response = $this->get('/api/1.0/users', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertCount(5, $parseData['data']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function userSuccessfullyCreated()
    {
        $data = ['first_name' => 'Foo', 'last_name' => 'Bar', 'email' => 'foo@bar.com'];
        $response = $this->post('/api/1.0/users', $data, $this->headers());

        $response->assertStatus(201)->assertJson([
            'message' => 'Created',
            'data' => null
        ]);
    }

    /**
     * @test
     */
    public function creatingUserFailedBecauseEmailNotSpecified()
    {
        $data = ['first_name' => 'Foo', 'last_name' => 'Bar', 'email' => ''];
        $response = $this->post('/api/1.0/users', $data, $this->headers());

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function creatingUserFailedBecauseUserAlreadyExist()
    {
        $data = ['first_name' => 'Foo', 'last_name' => 'Bar', 'email' => 'example1@gmail.com'];
        $response = $this->post('/api/1.0/users', $data, $this->headers());

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function oneUserFounded()
    {
        $response = $this->get('/api/1.0/users/1', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertEquals('Name1', $parseData['data']['first_name']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function userNotFounded()
    {
        $response = $this->get('/api/1.0/users/125', $this->headers());
        $response->assertStatus(200)->assertJson([
            'message' => 'OK',
            'data' => []
        ]);
    }

    /**
     * @test
     */
    public function userSuccessfullyUpdate()
    {
        $this->put('/api/1.0/users/1?first_name=Foo', [], $this->headers());
        $response = $this->get('/api/1.0/users/1', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertEquals('Foo', $parseData['data']['first_name']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function updatingUserFailedBecauseNotExist()
    {
        $response = $this->put('/api/1.0/users/125?first_name=Foo', [], $this->headers());
        $response->assertStatus(422)->assertJson([
            'message' => 'Unprocessable entity',
            'data' => null
        ]);
    }

    /**
     * @test
     */
    public function userSuccessfullyDeleted()
    {
        $response = $this->delete('/api/1.0/users/1', [], $this->headers());
        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function userCannotDeleteBecauseItNotExist()
    {
        $this->delete('/api/1.0/users/1', [], $this->headers());
        $response = $this->delete('/api/1.0/users/1', [], $this->headers());
        $response->assertStatus(422)->assertJson([
            'message' => 'Unprocessable entity',
            'data' => null
        ]);
    }

    private function headers()
    {
        return [
            ApiAuthentication::API_KEY => config('services.api.token'),
            'Accept' => 'application/json'
        ];
    }

    private function createUsers()
    {
        for ($i = 1; $i < 6; $i++) {
            $data = [
                'first_name' => 'Name' . $i,
                'last_name' => 'Last' . $i,
                'email' => "example$i@gmail.com"
            ];
            $this->post('/api/1.0/users', $data, $this->headers());
        }
    }

}
