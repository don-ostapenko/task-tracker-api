<?php

namespace Tests\Feature;

use App\Http\Middleware\ApiAuthentication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{

    use RefreshDatabase;
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->createUsers();
        $this->createTasks();
        $this->createTaskStatuses();
    }

    /**
     * @test
     */
    public function authenticationFailed()
    {
        $response = $this->get('/api/1.0/tasks');
        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function tasksFounded()
    {
        $response = $this->get('/api/1.0/tasks', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertCount(5, $parseData['data']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function tasksByFilterFounded()
    {
        $response = $this->get('/api/1.0/tasks?status=new&sort_by=id:desc', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertCount(2, $parseData['data']);
        $this->assertEquals(2, $parseData['data'][1]['id']);
        $this->assertEquals('new', $parseData['data'][1]['status']['code']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function allTasksFoundedNotSpecifiedParametersInQuery()
    {
        $response = $this->get('/api/1.0/tasks?status=&sort_by=', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertCount(5, $parseData['data']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function taskSuccessfullyCreated()
    {
        $data = ['title' => 'Foo', 'description' => 'Bar', 'status_id' => 1];
        $response = $this->post('/api/1.0/tasks', $data, $this->headers());

        $response->assertStatus(201)->assertJson([
            'message' => 'Created',
            'data' => null
        ]);
    }

    /**
     * @test
     */
    public function oneTaskFounded()
    {
        $response = $this->get('/api/1.0/tasks/1', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertEquals('Title1', $parseData['data']['title']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function taskNotFounded()
    {
        $response = $this->get('/api/1.0/tasks/125', $this->headers());
        $response->assertStatus(200)->assertJson([
            'message' => 'OK',
            'data' => []
        ]);
    }

    /**
     * @test
     */
    public function taskSuccessfullyUpdate()
    {
        $this->put('/api/1.0/tasks/1?title=Foo', [], $this->headers());
        $response = $this->get('/api/1.0/tasks/1', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertEquals('Foo', $parseData['data']['title']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function taskCannotBeUpdateBecauseItNotExist()
    {
        $response = $this->put('/api/1.0/tasks/125?title=Foo', [], $this->headers());
        $response->assertStatus(422)->assertJson([
            'message' => 'Unprocessable entity',
            'data' => null
        ]);
    }

    /**
     * @test
     */
    public function taskSuccessfullyPatched()
    {
        $data = ['status_id' => 2, 'user_id' => 2];
        $response = $this->patch('/api/1.0/tasks/1/patch', $data, $this->headers());

        $response->assertStatus(204);

        $response = $this->get('/api/1.0/tasks/1', $this->headers());
        $parseData = \json_decode($response->getContent(), true);

        $this->assertEquals('2', $parseData['data']['user']['id']);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function taskCannotBePatchedBecauseItNotExist()
    {
        $data = ['status_id' => 2, 'user_id' => 2];
        $response = $this->patch('/api/1.0/tasks/125/patch', $data, $this->headers());
        $response->assertStatus(422)->assertJson([
            'message' => 'Unprocessable entity',
            'data' => null
        ]);
    }

    /**
     * @test
     */
    public function taskSuccessfullyDeleted()
    {
        $response = $this->delete('/api/1.0/tasks/1', [], $this->headers());
        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function taskCannotDeleteBecauseItNotExist()
    {
        $this->delete('/api/1.0/tasks/1', [], $this->headers());
        $response = $this->delete('/api/1.0/tasks/1', [], $this->headers());
        $response->assertStatus(422)->assertJson([
            'message' => 'Unprocessable entity',
            'data' => null
        ]);
    }

    private function headers()
    {
        return [
            ApiAuthentication::API_KEY => config('services.api.token'),
            'Accept' => 'application/json'
        ];
    }

    private function createTasks()
    {
        for ($i = 1; $i <= 2; $i++) {
            $data = [
                'title' => 'Title' . $i,
                'description' => 'Description' . $i,
                'status_id' => 1,
                'user_id' => 1
            ];
            $this->post('/api/1.0/tasks', $data, $this->headers());
        }

        for ($i = 1; $i <= 3; $i++) {
            $data = [
                'title' => 'Title' . $i,
                'description' => 'Description' . $i,
                'status_id' => 2,
                'user_id' => 1
            ];
            $this->post('/api/1.0/tasks', $data, $this->headers());
        }
    }

    private function createUsers()
    {
        for ($i = 1; $i < 6; $i++) {
            $data = [
                'first_name' => 'Name' . $i,
                'last_name' => 'Last' . $i,
                'email' => "example$i@gmail.com"
            ];
            $this->post('/api/1.0/users', $data, $this->headers());
        }
    }

    private function createTaskStatuses()
    {
        $this->post('/api/1.0/task-statuses', ['name' => 'New', 'code' => 'new'], $this->headers());
        $this->post('/api/1.0/task-statuses', ['name' => 'View', 'code' => 'view'], $this->headers());
    }

}
